#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int ispermutation (char *, char *);
void countchars (char *, int *); 

int 
main (int argc, char **argv)
{
	char *string1 = argv[1];
	char *string2 = argv[2];

	printf ("Are %s and %s are permutations? %s\n", string1, string2, (ispermutation (string1, string2)? "yes\0" : "no\0"));	
}

int 
ispermutation (char *a, char *b)
{
	if (strlen (a) != strlen (b))
		return 0; 

	int achars[26] = { 0 };
	int bchars[26] = { 0 };
	int i; 

	countchars(a, achars);
	countchars(b, bchars);

	for (i = 0; i < 26; i++) {
		printf ("%d = a, %d = b\n", achars[i], bchars[i]);
		if (achars[i] != bchars[i]) {
			return 0; 
		}
	}

	return 1; 
} 

void
countchars (char *str, int *chars)
{
	int i;

	for (i = 0; i < strlen (str); i++) {
		chars[str[i] - 'a']++; 
	}
}