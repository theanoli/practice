#include "bin_trees.h"

// call insert 3 times passing it the root pointer to build the tree

struct node *build123 ();
struct node *insert ( struct node*, int ); 

int 
main ( void )
{
	struct node *tree; 
	int i; 
	int tree_size; 
	int depth;
	int min, max; 

	tree = build123 ();

	for ( i = 0; i < 7; i++ ) {
		insert ( tree, drand48 () * 10 );
	}

	// printPostorder ( tree );
	// printf ("\n");
	printNodes ( tree );
	
	tree_size = size ( tree ); 
	printf ("Your tree has %d nodes!\n", tree_size); 

	depth = maxDepth ( tree );
	printf ("Max tree depth is %d.\n", depth);

	min = minValue ( tree );
	max = maxValue ( tree ); 	
	printf ("Min tree value is %d, max is %d.\n", min, max);

	freeNodes ( tree );
}


struct node *
build123 () 
{
	struct node *tree;

	tree = insert ( NULL, 2 );
	insert ( tree, 1 );
	insert ( tree, 3 );

	return tree; 
}


struct node *
insert (struct node* node, int data) 
{
	if ( node == NULL ) {
		return ( newNode (data) );
	} else {
		if ( data <= node->data ) {
			node->left = insert ( node->left, data ); 
		} else {
			node->right = insert ( node->right, data );
		}

		return ( node );
	}
}

