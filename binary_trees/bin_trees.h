#include <stdlib.h>
#include <stdio.h>

#define max(X, Y) (((X) > (Y)) ? (X) : (Y))

// Problems from Stanford CS Education Library Binary Tree tutorial

struct node {
	int data; 
	void *left; 
	void *right; 
};


void 
printNodes ( struct node *node )
{
	if ( node == NULL ) 
		return; 

	printNodes (node->left);
	printf ("%i\n", node->data); 
	printNodes (node->right);
}


struct node *
newNode ( data ) 
{
	struct node *node = malloc (sizeof (struct node));

	node->data = data; 
	node->left = NULL; 
	node->right = NULL; 

	return node; 
}


void 
freeNodes ( struct node* node ) 
{
	if ( node == NULL ) 
		return; 

	freeNodes ( node->left );
	freeNodes ( node->right );

	// printf ("Freeing %i!\n", node->data);
	free ( node );
}


int 
size ( struct node *node ) 
{
	// no need to use a counter here; can just sum "in place"
	if ( node == NULL ) 
		return 0; 
	else 
		// Include count for this node (+1)
		return (size (node->left) + 1 + size (node->right));
}


void 
printPostorder(struct node *node) 
{

	if ( node == NULL ) 
		return;

	printNodes (node->left);
	printNodes (node->right);

	printf ("%d\n", node->data);
}


int 
maxDepth (struct node *node) 
{
	if ( node == NULL )
		return 0; 

	int depth = 1; 

	depth += max (maxDepth (node->left), maxDepth (node->right));

	return depth; 

}


int
minValue (struct node *node)
{
	if ( node == NULL )
		return 0;

	int min = node->data; 

	min = minValue (node->left);

	return min; 
}


int 
maxValue (struct node *node) 
{
	if ( node == NULL )
		return 0;

	int max = node->data; 

	max = maxValue (node->right);

	return max; 
}



