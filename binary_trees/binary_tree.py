# Basic binary search tree implementation


import random

# Random number generator
def nrand():
	return int(random.random() * 10)


class Tree(object):
	def __init__(self):
		self.root = None
		self.size = 0

	# Returns a node
	def _insert(self, node, new_node):
		# Terminating condition; this is when it hits a leaf node
		if node == None:
			self.size += 1
			return new_node

		# Not at a leaf node yet; check if we need to go left or right
		if new_node.data <= node.data: 
			node.left = self._insert(node.left, new_node)
		else:
			node.right = self._insert(node.right, new_node)

		# Return the root node
		return node

	# Public + private function keeps syntax of public function simpler
	def insert(self, new_node):
		self.root = self._insert(self.root, new_node)


class Node(object):
	def __init__(self, data): 
		self.data = data
		self.left = None
		self.right = None


def print_tree(node):
	# Terminating condition: leaf node
	if node == None:
		return

	# Print left, then node, then right
	else: 
		print_tree(node.left)
		print node.data
		print_tree(node.right)


def print_postorder(node):
	if node == None: 
		return

	else: 
		print_tree(node_left)
		print node.data
		print_tree(node.right)


tree = Tree()

node = Node(12)


for i in range(10):
	num = nrand()
	print "Random number: " + str(num)
	tree.insert(Node(num))

tree.insert(node)

print_tree(tree.root)
print tree.size