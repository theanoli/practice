#include <stdlib.h>
#include <stdio.h>

// call newNode 3 times, use 3 pointer variables

struct node {
	int data; 
	void *left; 
	void *right; 
};


struct node *build123 ();
struct node *newNode ( int data );
void printNodes ( struct node * );

int 
main ( void )
{
	struct node *node; 

	node = build123 (); 

	printNodes ( node );
}


void 
printNodes (struct node *node)
{
	if ( node == NULL ) {
		return; 
	} else {
		printf ("%i\n", node->data); 
		if ( node->left != NULL)
			printNodes (node->left);
		if ( node->right != NULL )
			printNodes (node->right);
	}
}


struct node *
newNode ( data ) 
{
	struct node *node = malloc (sizeof (struct node));

	node->data = data; 
	node->left = NULL; 
	node->right = NULL; 

	return node; 
}


struct node *
build123 () 
{
	struct node *two, *three, *one; 

	two = newNode ( 2 );
	three = newNode ( 3 ); 
	one = newNode ( 1 );

	two->left = one; 
	two->right = three; 

	return two; 
}