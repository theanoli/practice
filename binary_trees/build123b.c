#include "bin_trees.h"

// call newNode 3 times, use 1 pointer variable

struct node *build123 ();

int 
main ( void )
{
	struct node *tree; 

	tree = build123 (); 

	printNodes ( tree );
	freeNodes ( tree );
}


struct node *
build123 () 
{
	struct node *two;

	two = newNode ( 2 );

	two->left = newNode ( 1 ); 
	two->right = newNode ( 3 ); 

	return two; 
}


