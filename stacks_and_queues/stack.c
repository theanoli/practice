#include "../std_headers.h"


// Last in, first out
struct stack_node {
	int data; 
	struct stack_node *next; 
}; 

struct stack {
	struct stack_node *top;
};


void push ( int, struct stack * );
void pop ( struct stack * ); 
void peek ( struct stack * ); 


int
main (void)
{
	struct stack *stack; 
	int i; 

	stack = malloc ( sizeof (struct stack) ); 

	stack->top = NULL; 

	for ( i = 0; i < 10; i++ ) {
		printf ("Pushing %d onto the stack...\n", i);
		push ( i, stack ); 
	}

	// Check that it's working...
	peek ( stack );  // 9
	push ( 12, stack ); 
	peek ( stack );  // 12
	pop ( stack ); pop ( stack );
	peek ( stack );  // 8
	pop ( stack ); pop ( stack );
	peek ( stack );  // 6

	while ( stack->top != NULL ) {
		struct stack_node *current = stack->top; 
		stack->top = current->next; 
		printf ("Freeing %d\n", current->data); 
		free ( current ); 
	}
}


void
push ( int data, struct stack *stack ) 
{
	struct stack_node *node; 
	node = malloc (sizeof (struct stack_node));

	// set the node's next field to the current top, then
	// set stack's top to current node
	if ( node != NULL ) {
		node->next = stack->top; 
		stack->top = node; 

		node->data = data; 
	} else {
		printf ("Malloc failed! Exiting...");
		exit (0);
	}
}


void 
pop ( struct stack *stack )
{
	struct stack_node *to_pop; 

	to_pop = stack->top; 
	stack->top = to_pop->next; 

	free ( to_pop ); 
}


void
peek ( struct stack *stack )
{
	printf ("Peek %d\n", stack->top->data); 
}