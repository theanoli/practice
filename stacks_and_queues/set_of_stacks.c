#include "../std_headers.h"

#define MAX_NODES 10

struct set_of_stacks {
	struct stack *current; 
};

struct stack {
	struct node *top; 
	struct stack *next; 
	int size; 
};

struct node {
	struct node *next; 
	int data; 
};


void push ( int, struct stack ** ); 
void peek ( struct stack * ); 


int
main ( void )
{
	struct stack *set_of_stacks; 
	struct stack *stack; 
	int i; 

	stack = malloc ( sizeof (struct stack) );

	set_of_stacks = stack; 
	stack->top = NULL; 

	for ( i = 0; i < 15; i++ ) {
		printf ("Pushing %d onto the stack...\n", i);
		push ( i, &set_of_stacks ); 
	}

	// while ( set_of_stacks->top != NULL ) {
	// 	struct stack_node *current = set_of_stacks->top; 
	// 	set_of_stacks->top = current->next; 
	// 	printf ("Freeing %d\n", current->data); 
	// 	free ( current ); 
	// }
}


void 
push (int data, struct stack **set_of_stacks) 
{
	struct stack *curr_stack = *set_of_stacks; 
	struct node *node; 

	printf ("Current stack size: %d\n", curr_stack->size); 

	if ( MAX_NODES == curr_stack->size ) {
		printf ("Stack size too large; creating a new one...\n");

		struct stack *new_stack = malloc ( sizeof (struct stack) ); 
		new_stack->next = curr_stack; 
		new_stack->size = 0; 

		curr_stack = new_stack; 
		*set_of_stacks = new_stack; 
	}

	node = malloc ( sizeof (struct node) ); 

	if ( node != NULL ) {
		node->data = data; 
		node->next = curr_stack->top; 

		curr_stack->top = node->next; 
		curr_stack->size++; 
	}

}


void 
peek (struct stack *set_of_stacks)
{
	struct node *node = set_of_stacks->top; 

	printf ("Peeking at %d...\n", node->data); 
}






