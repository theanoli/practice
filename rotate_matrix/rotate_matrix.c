#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int n = 4; 

void rotate_matrix (int matrix[][n]);
void transpose (int matrix[][n]);
void swap_columns (int matrix[][n]);


int
main (void) 
{
	int i, j; 

	int matrix[4][4] = { 	{1, 2, 3, 4}, 
							{5, 6, 7, 8}, 
							{9, 10, 11, 12}, 
							{13, 14, 15, 16} 
						};

	rotate_matrix (matrix); 

	for ( i = 0; i < n; i++ ) {
		for ( j = 0; j < n; j++ ) {
			printf ("%d ", matrix[i][j]);
		}
		printf ("\n");
	}
}

void 
rotate_matrix (int matrix[][n]) 
{
	transpose (matrix); 
	swap_columns (matrix); 
}

void
transpose (int matrix[][n])
{
	int temp; 
	int i, j; 

	for ( i = 0; i < n; i++ ) {
		for ( j = 0; j < i; j++ ) {
			temp = matrix[j][i];
			matrix[j][i] = matrix[i][j];
			matrix[i][j] = temp; 
		}
	}
}

void 
swap_columns (int matrix[][n])
{
	int temp; 
	int i, j;  

	for ( i = 0; i < n; i++ ) {
		for ( j = 0; j < (n/2); j++ ) {
			temp = matrix[i][n - 1 - j]; 
			matrix[i][n - 1 - j] = matrix[i][j];
			matrix[i][j] = temp; 
		}
	}
}