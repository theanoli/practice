#  In one mode of the grafix software package, the user blocks off portions of a masking layer using opaque rectangles. The bitmap used as the masking layer is 400 pixels tall and 600 pixels wide. Once the rectangles have been blocked off, the user can perform painting actions through the remaining areas of the masking layer, known as holes. To be precise, each hole is a maximal collection of contiguous pixels that are not covered by any of the opaque rectangles. Two pixels are contiguous if they share an edge, and contiguity is transitive.

# You are given a String[] named rectangles, the elements of which specify the rectangles that have been blocked off in the masking layer. Each String in rectangles consists of four integers separated by single spaces, with no additional spaces in the string. The first two integers are the window coordinates of the top left pixel in the given rectangle, and the last two integers are the window coordinates of its bottom right pixel. The window coordinates of a pixel are a pair of integers specifying the row number and column number of the pixel, in that order. Rows are numbered from top to bottom, starting with 0 and ending with 399. Columns are numbered from left to right, starting with 0 and ending with 599. Every pixel within and along the border of the rectangle defined by these opposing corners is blocked off.

# Return a int[] containing the area, in pixels, of every hole in the resulting masking area, sorted from smallest area to greatest. 

# Recall list.insert(0, x) adds to beginning of list. list.append equiv to push. list.pop removes from end

class Node(object):
	def __init__(self, i, j):
		self.i = i
		self.j = j


def create_grid(rectangles):
	# argument is list of rectangles
	grid = [[0 for i in range(grid_width)] for j in range(grid_height)]

	for rectangle in rectangles: 
		(top, left, bottom, right) = rectangle
		
		for i in range(top, bottom + 1):
			for j in range(left, right + 1):
				grid[i][j] = 1

	return grid


def fill_area(node):
	count = 0

	stack = []
	stack.append(node)

	while len(stack) > 0:
		top = stack.pop()

		# Need to check whether it's in bounds and whether it's been filled
		if ((top.i < 0) or (top.i >= grid_height)): 
			continue
		if ((top.j < 0) or (top.j >= grid_width)):
			continue
		if (grid[top.i][top.j]):
			continue

		# Fill and count the node
		grid[top.i][top.j] = True
		count += 1

		# Push all the nodes around this one to the stack
		stack.append(Node(top.i + 1, top.j))  # to the right
		stack.append(Node(top.i - 1, top.j))  # to the left
		stack.append(Node(top.i, top.j + 1))  # above
		stack.append(Node(top.i, top.j - 1))  # below

	return count


def collect_areas(grid):

	result = []

	for i in range(grid_height):
		for j in range(grid_width):
			if grid[i][j] == False:
				result.append(fill_area(Node(i, j)))

	return result


if __name__ == "__main__":
	import sys

	grid_height = 400
	grid_width = 600
	
	rectangles = sys.argv[1].split(",")
	rectangles = [x.strip('{" "}') for x in rectangles]
	rectangles = [x.split(" ") for x in rectangles]
	rectangles = [[int(y) for y in x] for x in rectangles]

	
	grid = create_grid(rectangles)

	result = collect_areas(grid)

	print result



