#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct {
	char c; 
	int nc; 
} char_count_t;

void string_compr (char *);
char_count_t count_chars (char *);

int 
main (int argc, char **argv) 
{
	char *str = argv[1];

	string_compr (str);
}

void string_compr (char *str) 
{
	size_t length = strlen(str);
	int i; 
	int char_idx; 
	int count; 
	char_count_t *char_counts;

	char_counts = malloc (length * sizeof (char_count_t));


	char_counts[0].c = str[0]; 
	char_counts[0].nc = 0; 

	for (i = 0, char_idx = 0; i < length; i++ ) {
		char_counts[char_idx].nc++;

		if ( str[i] != str[i + 1] ) {
			char_counts[char_idx + 1].nc = 0; 
			char_counts[char_idx + 1].c = str[i + 1]; 
			char_idx++;
		}
	}

	if ( length < (char_idx * 2) ) {
		printf ("%s\n", str);
		free (char_counts);
		return; 
	}

	for ( i = 0; i < char_idx; i++ ) {
		printf ("%c%d", char_counts[i].c, char_counts[i].nc);
	}

	free (char_counts);
}


