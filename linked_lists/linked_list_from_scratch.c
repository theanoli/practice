#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct node_t {
	int data; 
	struct node_t *next; 
}; 


struct node_list_t {
	struct node_t *head; 
	struct node_t *tail; 
	size_t size; 
}; 

struct node_list_t *new_list (); 
struct node_t *push_node (struct node_list_t *, int); 
struct node_t *new_node (int);
void insert_node (struct node_t *, struct node_t *, struct node_list_t *); 
void remove_node (struct node_t *, struct node_list_t *);
void destroy_list (struct node_list_t *); 


int
main (void) 
{
	int i; 
	struct node_list_t *list;
	struct node_t *node; 
	struct node_t *five;
	struct node_t *eight; 

	list = new_list ();

	for ( i = 0; i < 10; i++ ) {
		node = push_node (list, i);
		printf ("%d in a %zu-node-long list\n", node->data, list->size);
		if ( i == 5 ) {
			five = node; 
		}

		if ( i == 8 ) {
			eight = node; 
		}
	}

	insert_node ( new_node (12), five, list );
	remove_node ( eight, list);

	struct node_t *current = list->head; 

	while ( current != NULL ) {
		printf ("%d in a(n) %zu-node-long list\n", current->data, list->size);
		current = current->next; 
	}

	destroy_list (list); 

}


// Create new list
struct node_list_t *
new_list ()
{
	struct node_list_t *list; 

	list = malloc ( sizeof (struct node_list_t) );

	if ( list != NULL ) {
		list->head = NULL;
		list->tail = NULL;
		list->size = 0; 
	}

	return list; 
}

// Create node, push to front of list
struct node_t *
new_node (int data)
{
	struct node_t *node; 

	node = malloc ( sizeof (struct node_t) );

	if ( node != NULL ) {
		node->data = data; 
	}

	return node; 
}


// Push node to front of list
struct node_t *
push_node (struct node_list_t *list_ptr, int data)
{
	struct node_t *node; 

	node = new_node (data);

	if ( node != NULL ) {
		node->next = list_ptr->head; 
	}

	list_ptr->head = node; 
	list_ptr->size++; 

	return node; 
}


// Insert node after specified node
void
insert_node (struct node_t *node, struct node_t *dest, struct node_list_t *list) 
{
	if ( node != NULL ) {
		node->next = dest->next; 
		dest->next = node; 
		list->size++; 
	}
}


// Remove node
void 
remove_node (struct node_t *node, struct node_list_t *list)
{
	struct node_t *current = list->head; 
	struct node_t *prev = NULL;  

	while ( current->data != node->data ) {
		prev = current; 
		current = current->next; 
	}

	prev->next = current->next; 

	free ( node );

	list->size--;
}



// Destroy list
void 
destroy_list (struct node_list_t *list)
{
	struct node_t *next_node = list->head; 
	struct node_t *current = next_node; 

	while ( current->next != NULL ) {
		printf ("Freeing %d...\n", next_node->data);
		current = next_node; 

		next_node = next_node->next; 
		free ( current ); 
	}
}



