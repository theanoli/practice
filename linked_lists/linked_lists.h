# include <stdlib.h>
# include <stdio.h>
# include <string.h>

typedef struct {
	struct node *next;
	void *data; 
} node_t; 

typedef struct {
	node_t *head; 
	int has_dummy_head;
	size_t size; 
} node_list_t; 


//*** Helpers ***//

node_t *
new_node (void *data, node_t *next) 
{
	node_t *rv = malloc ( sizeof (*rv) );

	if ( rv != NULL ) {  // i.e., the malloc worked
		rv->data = data; 
		rv->next = next; 
	}

	return rv; 
}



// New list with optional dummy head; returns pointer to new list, 
// or NULL if error

node_list_t *
new_list (int has_dummy_head) 
{
	node_list_t *rv = malloc ( sizeof *rv );

	if ( rv != NULL ) {  // deal with sentinel node if needed; otherwise just return rv
		rv->head = has_dummy_head? new_node ( NULL, NULL ) : NULL; 
		rv->has_dummy_head = has_dummy_head; 
		rv->size = 0; 

		if ( has_dummy_head && rv->head == NULL ) {
			// release list if a dummy couldn't be allocated
			free ( rv ); 
			rv = NULL; 
		}
	}

	return rv; 
}


// Destroy a given node, assuming it has been unlinked
// Optionally, destroy the data contained in the node 
// Returns the next node specified by the link 

node_t *
destroy_node ( node_t *node, void (destroy_data) ( void * ) ) 
{
	node_t *rv = NULL; 

	if ( node != NULL ) {
		rv = node->next; 

		if ( destroy_data != NULL )
			destroy_data ( node->data );

		free ( node );
	}

	return rv; 
}


// Destroy all nodes in a given list

void 
destroy_list ( node_list_t *list, void (destroy_data) ( void * ) ) 
{
	while ( list->head != NULL )
		list->head = destroy_node ( list->head, destroy_data );
}


// Insert a new node after the given node 

node_t *
insert_after ( node_list_t *list, node_t *pos, void *data ) 
{
	node_t *rv = NULL; 

	if ( (list != NULL) && (pos != NULL) ) {
		// Create a new node, set the next link
		rv = new_node ( data, pos->next );

		if ( rv != NULL ) {
			pos->next = rv; 
			list->size++;
		}
	}
	
	return rv; 
}

