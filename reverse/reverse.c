#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void reverse (char *);

int main(int argc, char *argv[]) 
{
	if (argc != 2) {
		printf ("Need a string argument!");
		exit(1);
	}

	char *string = argv[1];

	printf ("Your original string is \"%s\"\n", string);

	reverse(string);

	printf ("Now your string is \"%s\"\n", string);
}

void reverse(char *str) 
{
	int i;
	int max_idx;
	char temp; 

	max_idx = strlen (str) - 1;
	for (i = 0; i < max_idx; i++) {
		if ( i <= max_idx/2 ) {
			temp = str[max_idx - i];
			str[max_idx - i] = str[i];
			str[i] = temp; 
		} else {
			return;
		}
	}
}