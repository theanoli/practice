### Insertion sort ###
# Think of arranging a deck of cards into ascending order: for each card, starting
# at the leftmost card, put it to the left of any cards smaller than it. 
# O(n^2) worst-case complexity: O(n^2) comparisons & swaps
# O(n) best-case complexity: O(n) comparisons, O(1) swaps
# Good for small datasets, datasets which are already substantially sorted
# Constant memory usage (O(1))
# Can sort list as list is received

import sys

def insertion_sort(sort_list):
	sort_list = [int(float(x)) for x in sort_list.split(",")]
	length = len(sort_list)

	for i in range(1,length):
		print i
		key = sort_list[i]
		j = i - 1

		# Check your key against its immediate left-hand neighbor; if neighbor is 
		# larger, increment its index, check next left-hand neighbor until you
		# reach a smaller left-hand element, then park your key in front of it. 
		while key < sort_list[j] and j >= 0:
			sort_list[j + 1] = sort_list[j]
			j -= 1

		sort_list[j + 1] = key

	print sort_list

if __name__ == "__main__":
	insertion_sort(sys.argv[1])