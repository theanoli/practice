### Bubble sort algorithm ###
# O(n^2) performance worst case
# O(n) best case
# Bad for large sets, reverse-ordered sets
# Better than some other sorts b/c it checks as it runs whether list is already sorted 
# Can reduce comparisons by noting that the elements after last swap are already in order

import sys


def main (sort_list):
	sorted = False
	step_counter = 0

	sort_list = [int(float(x)) for x in sort_list.split(',')]

	length = len(sort_list) - 1
	print "sort_list has length %d." % length + 1

	# Go through the sort process until sorted remains true
	while False == sorted: 

		sorted = True

		for i in range(length): 

			if sort_list[i] > sort_list[i + 1]: 
				sorted = False
				sort_list[i], sort_list[i + 1] = sort_list[i + 1], sort_list[i]

		# Check sorted status to avoid counting last step
		step_counter += 1

	print "List was sorted in %d step(s)." % step_counter
	print sort_list


if __name__ == "__main__":
	main(sys.argv[1])
