#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void repl_spaces (char *, int);
void shift_by_two (char *, char *);

int
main (int argc, char **argv) {

	char *argument = argv[1];
	char string_array[30] = { 0 };
	
	memcpy (string_array, argument, strlen (argument));

	printf ("This is your string before: %lu chars: %s\n", strlen (argument), string_array);

	repl_spaces (string_array, strlen (argument));

	printf ("This is your string after: %s\n", string_array);
}


void 
repl_spaces (char *str, int length) 
{
	int i; 
	int chars_left = length; 
	char *pct_twenty = "\%20"; 


	for ( i = 0; i < length; i++, chars_left--) {
		if ( str[i] == ' ' ) {

			printf ("String before shifting: %s\n", str);
			shift_by_two (&str[i + 1], &str[i + 1 + chars_left]);
			printf ("String after shifting: %s\n", str);

			memcpy (&str[i], pct_twenty, 3); 
		
			length += 2; 
			i += 2; 
		}
	}
}

void
shift_by_two (char *start_ptr, char *end_ptr)
{
	int i; 

	for ( i = (end_ptr - start_ptr); i >= 0; i-- ) {
		start_ptr[i + 2] = start_ptr[i]; 
	}
}