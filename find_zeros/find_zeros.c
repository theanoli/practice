#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#define N 4 
#define M 6

void find_zeros (int[N][M]);
void set_zeros (int[N][M], int[], int[]);

int
main (void) 
{
	int i, j; 

	int matrix[N][M];

	for (i = 0; i < N; i++ ) {
		for ( j = 0; j < M; j++ ) {
			matrix[i][j] = drand48 () * 10;
		}
	} 

	printf ("Before: \n");
	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < M; j++ ) {
			printf ("%i ", matrix[i][j]);
		}
		printf ("\n");
	}

	find_zeros (matrix); 

	printf ("\nAfter: \n");
	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < M; j++ ) {
			printf ("%i ", matrix[i][j]);
		}
		printf ("\n");
	}

}

void
find_zeros (int matrix[N][M])
{
	int i, j, k; 
	int rows[N] = { 0 }; 
	int cols[M] = { 0 };

	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < M; j++ ) {
			if ( matrix[i][j] == 0 ) {
				rows[i] = 1; cols[j] = 1; 
			}
		}
	}

	set_zeros (matrix, rows, cols);
}

void
set_zeros (int matrix[N][M], int rows[], int cols[]) 
{
	int i, j; 
	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < M; j++ ) {
			printf ("%d\n", j);

			if (cols[j]) {
				matrix[i][j] = 0; 
			}
		}
		if (rows[i]) {
			memset (matrix[i], 0, M * sizeof (int));
		} 
	}
}

